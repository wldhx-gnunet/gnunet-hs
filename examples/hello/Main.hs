-- | Get our own PId from the Hello service
module Main where

import Control.Concurrent (forkIO)
import Control.Concurrent.MVar
import Control.Monad
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as C
import Data.Bits
import Foreign.Ptr
import Foreign.C.Types
import Foreign.Marshal.Alloc
import Foreign.Marshal.Utils
import Foreign.Storable
import Network.GNUnet.Common
import Network.GNUnet.Configuration
import Network.GNUnet.Crypto
import Network.GNUnet.Scheduler
import Network.GNUnet.CADET
import qualified Network.GNUnet.CADET.Types as CADET
import qualified Network.GNUnet.MQ as MQ
import Bindings.GNUnet.GnunetCommon
import Bindings.GNUnet.GnunetCryptoTypes
import Bindings.GNUnet.GnunetHelloLib
import Bindings.GNUnet.GnunetSchedulerLib
import Bindings.GNUnet.GnunetTransportHelloService
import Bindings.GNUnet.GnunetMqLib
import Bindings.GNUnet.GnunetCadetService
import Bindings.GNUnet.GnunetProtocols

main :: IO ()
main = do
    logSetup "cadet-test" "DEBUG"

    cfg <- load "gnunet.conf"

    run $ do
        hello_handle <- newEmptyMVar
        our_pid <- newEmptyMVar
        putMVar hello_handle =<< flip (c'GNUNET_TRANSPORT_hello_get cfg c'GNUNET_TRANSPORT_AC_ANY) nullPtr =<< (
            mk'GNUNET_TRANSPORT_HelloUpdateCallback $ \_ our_hello_header -> do
                when (nullPtr == our_hello_header) $ c'GNUNET_SCHEDULER_shutdown >> undefined
                our_hello <- castPtr <$> c'GNUNET_copy_message our_hello_header
                c'GNUNET_TRANSPORT_hello_get_cancel =<< readMVar hello_handle
                alloca $ \our_pid' -> do
                    assert' =<< (== C'GNUNET_OK) <$> c'GNUNET_HELLO_get_id our_hello our_pid'
                    putMVar our_pid =<< peek our_pid'
            )
        void . forkIO $ logNocheck c'GNUNET_ERROR_TYPE_DEBUG =<< show <$> readMVar our_pid
