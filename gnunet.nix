{ mkDerivation, atomic-primops, base, bindings-gnunet, bytestring
, list-t, stdenv, stm, stm-containers, text, time
}:
mkDerivation {
  pname = "gnunet";
  version = "0.0.0.1";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [ base bindings-gnunet bytestring text ];
  executableHaskellDepends = [
    atomic-primops base bindings-gnunet bytestring list-t stm
    stm-containers time
  ];
  license = stdenv.lib.licenses.agpl3;
}
