{ pkgs ? import <nixpkgs> { overlays = [ (import <gnunet-nix/overlay.nix>) (import <gnunet-nix/haskell-overrides.nix>) ]; } }:
{
  gnunet-hs = import ./default.nix { inherit pkgs; };
}
