module Network.GNUnet.Common where

import Foreign.Ptr
import Foreign.C.String
import Bindings.GNUnet.GnunetCommon
import Control.Exception (assert)

assert' :: Monad m => Bool -> m ()
assert' = flip assert (return ())

logSetup :: String -> String -> IO ()
logSetup name loglevel =
    withCString name $ \name' ->
        withCString loglevel $ \loglevel' ->
            assert' =<< (== C'GNUNET_OK) <$> c'GNUNET_log_setup name' loglevel' nullPtr

logNocheck :: C'GNUNET_ErrorType -> String -> IO ()
logNocheck level msg = withCString msg $
    c'GNUNET_log_nocheck level
