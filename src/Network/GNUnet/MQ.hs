module Network.GNUnet.MQ (mkMessageHandler, msg) where

import Data.ByteString (ByteString)
import qualified Data.ByteString as B
import Foreign.Ptr
import Foreign.Storable
import Foreign.Marshal.Array
import Foreign.C.Types (CInt, CUShort)
import qualified Network.GNUnet.CADET.Types as CADET
import Bindings.GNUnet.GnunetCommon
import Bindings.GNUnet.GnunetCadetService
import Bindings.GNUnet.GnunetMqLib

mkMessageHandler :: --Storable a =>
    (Ptr C'GNUNET_CADET_Channel -> C'GNUNET_MessageHeader -> IO CInt) ->
    (Ptr C'GNUNET_CADET_Channel -> C'GNUNET_MessageHeader -> ByteString -> IO ()) ->
    CUShort ->
--    a ->
    IO C'GNUNET_MQ_MessageHandler
mkMessageHandler validate handle msgTypeId {-msgType-} = do
    validate' <- mk'GNUNET_MQ_MessageValidationCallback $ mkValidate validate
    handle' <- mk'GNUNET_MQ_MessageCallback $ mkHandle handle
    return $ C'GNUNET_MQ_MessageHandler validate' handle' nullPtr msgTypeId (fromIntegral . sizeOf $ (undefined :: C'GNUNET_MessageHeader))--(fromIntegral . sizeOf $ msgType)

mkValidate :: (Ptr C'GNUNET_CADET_Channel -> C'GNUNET_MessageHeader -> IO CInt) -> (Ptr () -> Ptr C'GNUNET_MessageHeader -> IO CInt)
mkValidate f cls mh = do
    let ch = castPtr @_ @C'GNUNET_CADET_Channel cls
    mh' <- peek mh

    f ch mh'

mkHandle :: (Ptr C'GNUNET_CADET_Channel -> C'GNUNET_MessageHeader -> ByteString -> IO ()) -> (Ptr () -> Ptr C'GNUNET_MessageHeader -> IO ())
mkHandle f cls mh = do
    ch <- CADET.channel <$> (peek . castPtr $ cls)
    mh' <- peek mh

    c'GNUNET_CADET_receive_done ch

    let sz = sizeOf (undefined :: C'GNUNET_MessageHeader)
    let payload_sz = (fromIntegral . c'ntohs $ c'GNUNET_MessageHeader'size mh') - sz

    text <- B.packCStringLen (mh `plusPtr` sz, payload_sz)

    f ch mh' text

msg :: CUShort -> ByteString -> IO (Ptr C'GNUNET_MQ_Envelope)
msg msgTypeId content = B.useAsCStringLen content $ \(p, n) -> do
    let sz = sizeOf (undefined :: C'GNUNET_MessageHeader)
    envelope <- c'GNUNET_MQ_msg_ nullPtr (fromIntegral $ sz + n) msgTypeId
    mh <- c'GNUNET_MQ_env_get_msg envelope
    copyArray (mh `plusPtr` sz) p n
    return envelope
